var NewApp = angular.module('NewApp', ['ui.bootstrap','ngAnimate']);

NewApp.controller('mainCtrl', function($scope){
    $scope.sidebar = '1';
    $scope.slider = function(){
        if($scope.sidebar == '1'){
            $scope.sidebar= '0';
        }
        else if($scope.sidebar == '0'){
            $scope.sidebar= '1';
        }
        console.log($scope.sidebar);
    }
    $scope.images = [
        {
            src: 'img/1.jpg'
        },
        {
            src: 'img/2.jpg'
        },
        {
            src: 'img/3.jpg'
        }
    ];
});